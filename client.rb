#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>

# This should only use core ruby as it is used on non-standardized systems.
require 'net/http'

# HTTP server side event.
# Body looks like this:
#   event:stdout
#   data:hi there
#
class SSEvent
  attr_reader :name # This is a free form string.
  attr_reader :data # Free form data.

  def initialize(body)
    matchdata = /event:(?<name>.+)\ndata:(?<data>.*)/n.match(body)
    unless matchdata
      warn 'Failed to parse SSEvent'
      warn body.inspect
      return
    end
    @name = matchdata[:name].strip
    @data = matchdata[:data].strip
  end

  # Does whatever needs doing for this event.
  # stdout/err -> puts
  # error -> exit
  def run
    case name
    when 'error'
      if data && !data.empty?
        STDERR.puts data
        exit 1
      end
      exit 0
    when 'stderr' then STDERR.puts(data)
    else STDOUT.puts(data)
    end
  end
end

uri = URI.parse(ARGV.pop)
Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
  http.read_timeout = 60 * 60
  request = Net::HTTP::Get.new uri.request_uri
  http.request(request) do |response|
    response.read_body do |chunk|
      SSEvent.new(chunk).run
    end
  end
end
