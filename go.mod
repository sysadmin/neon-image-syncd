// SPDX-License-Identifier: CC0-1.0
// SPDX-FileCopyrightText: none

module invent.kde.org/sysadmin/neon-image-syncd.git

go 1.15

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/gin-gonic/gin v1.6.3
)
